@extends('layouts.app')

@section('content')
<br/>
    <h1>{{$post->title}}</h1>
    <div>
        {{$post->body}}
    </div>
    <br/>
    <a href="/posts" class="btn-default">Back</a>
    <hr>
    <small>Written on {{$post->created_at}}</small>
@endsection