<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <div class="container">
        <a href="/" class="navbar-brand">{{config('app.name','LSAPP')}}</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      
        <div class="collapse navbar-collapse" id="navbar">
          <ul class="nav navbar-nav">
                <li class="p-3"><a href="/"> Home</a></li>
                <li class="p-3"><a href="/about"> About</a></li>
                <li class="p-3"><a href="/services"> Services</a></li>
                <li class="p-3"><a href="/posts"> Blog</a></li>
          </ul>
          <ul class="nav navbar-nav pull-right">
            <li><a href="/posts/create">Create Post</a></li>
          </ul>
        </div>
    </div>
</nav>